#!/usr/bin/bash
# Kibana UB 18.04

apt-get install -y kibana=7.6.2

cd /usr/share/kibana/
sudo -u kibana bin/kibana-plugin install https://packages.wazuh.com/wazuhapp/wazuhapp-3.12.2_7.6.2.zip

echo "Edit /etc/kibana/kibana.yml"
