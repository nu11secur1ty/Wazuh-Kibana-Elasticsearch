#!/usr/bin/bash
#Elastic Stack UB 18.04

apt-get install -y  curl apt-transport-https
curl -s https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | tee /etc/apt/sources.list.d/elastic-7.x.list
apt-get update -y
apt-get install -y elasticsearch=7.6.2



